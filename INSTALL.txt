Readme
-----------

Take a look at the README.txt file for an introduction to this module.

Requirements
-----------

This module requires Drupal 4.7.x.

Installation
-----------

1. Create the SQL tables. This depends a little on your system, but the most common method is:
     mysql -u username -ppassword drupal < easylinks.mysql

2. Place the easylinks module with all its files under modules/easylinks.

3. Enable the module in administer >> modules and set the module settings to your liking in 
   administer >> settings >> easylinks.

-----------
Original Author: Dirk J. Botha <bothadj@wdsl.co.za>
Current Maintainer: Gurpartap http://drupal.org/user/41470

