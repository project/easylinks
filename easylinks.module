<?php

/**
 * Implementation of hook_help
 */
function easylinks_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return t('A simple links management module. Provides a links page with ability for allowed user roles to add links.');
    case 'easylinks':
      return t('A list of useful links. If you find that the link is broken, click on <img src="misc/watchdog-error.png" alt="(X)"></img> next to the link, in order to report it.');
    case 'admin/easylinks':
      return t('Administer a list of links.');
  }
}

/**
 * Implementation of hook_perm
 */
function easylinks_perm() {
  return array('view easylinks', 'suggest easylinks', 'edit easylinks', 'admin easylinks');
}

/**
 * Implementation of hook_menu
 */
function easylinks_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array('path' => 'easylinks', 'title' => t('links'),
	'callback' => 'easylinks_view', 'access' => user_access('view easylinks'));
    $items[] = array('path' => 'easylinks/suggest', 'title' => t('suggest a link'),
	'callback' => 'easylinks_suggest', 'access' => user_access('suggest easylinks'));
    $items[] = array('path' => 'admin/easylinks', 'title' => t('easylinks'),
	'callback' => 'easylinks_admin', 'access' => user_access('edit easylinks'));
  } else {
    if (arg(0) == 'easylinks' && is_numeric(arg(1))) {
      if(arg(2) == 'edit') {
      $items[] = array('path' => 'easylinks/'. arg(1) .'/edit', 'title' => t('link edit'),
        'callback' => 'easylinks_admin_edit', 'access' => user_access('edit easylinks'),
        'type' => MENU_LOCAL_TASK);
      }

      if(arg(2) == 'reportbroken') {
      $items[] = array('path' => 'easylinks/'. arg(1) .'/reportbroken', 'title' => t('Report broken link'),
        'callback' => 'easylinks_report_broken', 'access' => user_access('view easylinks'),
        'type' => MENU_LOCAL_TASK);
      }
    }
  }

  return $items;
}

/**
 * Implementation of hook_settings
 */
function easylinks_settings() {
  $form = easylinks_settings_form();
  return $form;
}

/**
 * Generates the settings form
 */
function easylinks_settings_form() {
  $form['links_rows_in_view'] = array(
    '#type' => 'textfield',
    '#title' => t('Links shown per View page'),
    '#default_value' => variable_get('links_rows_in_view', 10),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('The number of links to display per page on the View page.'),
  );

  $form['links_rows_in_edit'] = array(
    '#type' => 'textfield',
    '#title' => t('Links shown per Edit page '),
    '#default_value' => variable_get('links_rows_in_edit', 10),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('The number of links to display per page on the Edit page.'),
  );

  $form['links_rows_in_block'] = array(
    '#type' => 'textfield',
    '#title' => t('Links shown in Block'),
    '#default_value' => variable_get('links_rows_in_block', 10),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('The number of links to display in block.'),
  );

  $form['links_autoapprove'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto approve suggested links'),
    '#default_value' => variable_get('links_autoapprove', 0),
    '#description' => t('If this option is set, suggested links will automatically be approved and show in the view page.'),
  );

  $form['easylinks_target'] = array(
    '#type' => 'checkbox',
    '#title' => t('Open links in new window'),
    '#default_value' => variable_get('easylinks_target', 0),
    '#description' => t('If this option is set, suggested links on the view page, will open in new window.'),
  );

  $form['easylinks_sort_alphabetically'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sort links alphabetically'),
    '#default_value' => variable_get('easylinks_sort_alphabetically', 0),
    '#description' => t('If this option is set, suggested links on the view page and block, will be listed alphabetically.'),
  );

  return $form; 
}

/**
 * Helper function.
 * Returns list of link statuses.
 */
function easylinks_status_list() {
  return array(t('suggested'),t('approved'),t('reported broken'));
}

/**
 * Selects records from the easylinks table
 * @param $numrecs Number of records to retrieve at a time. (Default = 10)
 * @param $whereclause Where caluse you want for the query, without the "WHERE" keyword. (Default = "lid != 0")
 * @param $orderby Which field(s) you want the list ordered by. (Default = "l.lid")
 * Returns list of link statuses.
 */
function easylinks_select($numrecs = "10",$whereclause = "lid != 0",$orderby = "l.lid") {
  if(variable_get('easylinks_sort_alphabetically', 0)) {
    $orderby = "l.sitename";
  }
  $sql = "SELECT l.lid, l.state, l.uid, l.sugestdttm, l.url, l.sitename, l.sitedesc FROM {easylinks} l WHERE %s ORDER BY %s";

  $result = pager_query($sql, $numrecs, 0, NULL,$whereclause,$orderby);

  return $result;

}

/**
 * A test function.
 * Used to add 100 records to the database quickly.
 * Should not be used for anything but testing.
 */
//function easylinks_insert_test($linkdata) {
//  global $user;
//  $count = 0;
//
//  while ($count < 100) {
//
//    $curdttm = time();
//
//    $lurl  = $linkdata['links_url'];
//    $sname = "Test site " . $count;
//    $sdesc = "Test site " . $count . "description";
//
//    $sql = "INSERT INTO {easylinks}
//            (
//              state,
//              uid,
//              sugestdttm,
//              url,
//              sitename,
//              sitedesc
//            ) VALUES (
//              0,
//              $user->uid, 
//              $curdttm,
//              '$lurl',
//              '$sname',
//              '$sdesc'
//            )";
// 
//    db_query($sql);
//
//    $count++;
//  }
//  return 0;
//}

/**
 * Inserts a single record into the easylinks table.
 * @param $linkdata An assotiative array. Containign:
 *                  links_url : the url
 *                  links_sitename : the short name for the link
 *                  links_setedesc : the long description for the link
 */
function easylinks_insert($linkdata) {
  global $user;

  $curdttm = time();

  $lurl  = $linkdata['links_url'];
  $lstat = variable_get('links_autoapprove', 0);
  $sname = $linkdata['links_sitename'];
  $sdesc = $linkdata['links_sitedesc'];

//  $sql = "INSERT INTO {easylinks}
//          (
//            state,
//            uid,
//            sugestdttm,
//            url,
//            sitename,
//            sitedesc
//          ) VALUES (
//            $lstat,
//            $user->uid, 
//            $curdttm,
//            '$lurl',
//            '$sname',
//            '$sdesc'
//          )";
 
  db_query("INSERT INTO {easylinks} (state, uid, sugestdttm, url, sitename, sitedesc)
            VALUES ($lstat, $user->uid, $curdttm, '%s', '%s', '%s')", $lurl, $sname, $sdesc);
  return 0;
}

/**
 * Update a record in the easylinks table.
 * @param $linkdata An assotiative array. Containign:
 *                  links_state    : the state you want the link to have after update
 *                  links_url      : the url
 *                  links_sitename : the short name for the link
 *                  links_setedesc : the long description for the link
 */
function easylinks_update($linkdata) {
    db_query("UPDATE {easylinks} SET state = %d, url = '%s', sitename = '%s', sitedesc = '%s' WHERE lid = %d", $linkdata['links_state'], $linkdata['links_url'], $linkdata['links_sitename'], $linkdata['links_sitedesc'], $linkdata['links_lid']);
}

/**
 * Delete a record in the easylinks table.
 * @param $linkdata An assotiative array. Containign:
 *                  links_lid : the lid of the link in the easylinks table.
 */
function easylinks_delete($linkdata) {
    db_query("DELETE FROM {easylinks} WHERE lid = %d",$linkdata['links_lid']);
}

/**
 * Gets the current user's name from the user table
 * @param $userid The user's user id.
 */
function easylinks_get_username($userid) {
  $username = "Unknown";

//  $sql      = "SELECT u.name
//               FROM   {users} u
//               WHERE  uid = $userid";

  $result   = db_query("SELECT u.name FROM {users} u WHERE uid = %s",$userid);

  if (db_num_rows($result)) {
    $u = db_fetch_object($result);
    $username = $u->name;
  }
  return $username;
}

/**
 * Attempts to asscertain the existance of a URL by opening the file pointer to the site
 * @param $url The url. 
 */
function easylinks_is_url($url) {
  $valid_codes = array('200', '201', '202', '203', '204', '205', '206', '300', '301', '302', '303', '304', '305', '306', '307');
  // allow_url_fopen off is reality now because of security risk. such fopen test may produce warnings
  if (strtolower(ini_get("allow_url_fopen") == 'on')) {
    $fp = @fopen($url,"r");
    if ($fp) {
      fclose($fp);
      return true;
    } else {
      return false;
    }
  } else {
    // allow_url_fopen == off! we cannot check, let's try curl
    if (!function_exists('curl_init')) {
      // curl is not available, what now? return true, cannot check at all...
      return true;
    }
    $ch = curl_init();
    if (!$ch) {
      // curl failed, will not check! shouldn't we warn the user?
      return false;
    }
    $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $ret = curl_setopt($ch, CURLOPT_URL,            $url);
    $ret = curl_setopt($ch, CURLOPT_HEADER,        1);
    $ret = curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $ret = curl_setopt($ch, CURLOPT_TIMEOUT,        20);

    $ret = curl_exec($ch);
    if (empty($ret)) {
      // error happened, shouldn't we warn the user?
      curl_close($ch);
      return false;
    }
    $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    if (in_array($info, $valid_codes)) {
      return true;
    } else {
      return false;
    }
  }
}

/**
 * Helper function.
 * Quikly build an assotiative array from posted values.
 */
function easylinks_build_form_data() {
  $link->lid      = $_POST['edit']['links_lid'];
  $link->url      = $_POST['edit']['links_url'];
  $link->sitename = $_POST['edit']['links_sitename'];
  $link->sitedesc = $_POST['edit']['links_sitedesc'];
  $link->state    = $_POST['edit']['links_state'];

  return $link;
}

/**
 * Builds a form, all form elements common to suggesting a link, reporting a broken link
 * and editing a link are here.
 * @param $linkdata An assotiative array containing:
 *                  sitename, sitedesc, url
 *        $readonly Set to "readonly" if you want the elements to be readonly.
 */
function easylinks_common_form($linkdata,$readonly = "") {
  $form['links_sitename'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Name'),
    '#value' => $linkdata->sitename,
    '#size' => 30,
    '#maxlength' => 64,
    '#description' => t('The website\'s name.'),
  );

  $form['links_sitedesc'] = array(
    '#type' => 'textarea',
    '#title' => t('Site Description'),
    '#default_value' => $linkdata->sitedesc,
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('The text you type here will be displayed next to the link.'),
  );

  $form['links_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Site URL'),
    '#value' => $linkdata->url,
    '#size' => 70,
    '#maxlength' => 256,
    '#description' => t('The URL for the website. Please type the FULL URL. I.e. starting with http://'),
  );

  if ($readonly == "readonly") {
    $form['links_sitename']['#attributes'] = array('readonly' => 'readonly');
    $form['links_sitedesc']['#attributes'] = array('readonly' => 'readonly');
    $form['links_url']['#attributes'] = array('readonly' => 'readonly');
  }

  return $form; 
}

/**
 * Creates the easylinks suggest a link form
 */
function easylinks_suggest_form() {
  $linkdata = easylinks_build_form_data();
  $form     = easylinks_common_form($linkdata);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Suggest this link'));
  $output = drupal_get_form('easylinks_suggest_page', $form);
  return $output;
}

/**
 * Creates the easylinks edit link form
 * @param $linkdata An assotiative array containing:
 *                  All the eliments required for easylinks_common_form() and
 *                  state : the current state of the link.
 */
function easylinks_admin_edit_form($linkdata,$readonly = '') {
  $form   = easylinks_common_form($linkdata,$readonly);
  $optlst = easylinks_status_list();

  $form['links_state'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => $linkdata->state,
    '#options' => $optlst,
    '#description' => t('Select the new status of this link.'),
    '#readonly' => $readonly,
  );

  if ($readonly == "readonly") {
    $form['links_state']['#attributes'] = array('disabled' => 'disabled');
  }

  $form['links_lid'] = array('#type' => 'hidden', '#value' => $linkdata->lid);

  return $form;
}

/**
 * Creates the easylinks edit link form
 * @param $linkdata An assotiative array containing:
 *                  All the eliments required for easylinks_common_form() and
 *                  state : the current state of the link.
 */
function easylinks_report_broken_form($linkdata) {
  $form   = easylinks_common_form($linkdata,'readonly');
  $optlst = easylinks_status_list();

  $form['links_state'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => $linkdata->state,
    '#options' => $optlst,
    '#description' => t('Select the new status of this link.'),
  );

  $form['links_state']['#attributes'] = array('disabled' => 'disabled');
  $form['links_lid'] = array('#type' => 'hidden', '#value' => $linkdata->lid);

  return $form;
}

/**
 * Contains the logic around suggesting a new link
 */
function easylinks_suggest() {
  if ($_POST['op'] == t('Suggest this link')) {
    $validsite = easylinks_is_url($_POST['edit']['links_url']);

    if (is_string($_POST['edit']['links_sitename']) && strlen($_POST['edit']['links_sitename']) > 0) {
      if (is_string($_POST['edit']['links_sitedesc']) && strlen($_POST['edit']['links_sitedesc']) > 0) {
        if (!$validsite) {
          form_set_error('links_url', 'The URL for the site you are attempting to suggest is either not valid, or the site is down. Check your suggestion, if the URL is correct you might want to try again once the site is up!');
          $output = easylinks_suggest_form();
        } else {
//          easylinks_insert_test($_POST['edit']);
          easylinks_insert($_POST['edit']);
          drupal_set_message(t('Thank you for your suggestion. It has been forwarded to, and will be reviewed by the site administrator.'));
          drupal_goto('easylinks');
        }
      } else {
        form_set_error('links_sitedesc', 'You must enter a description for the site you are suggesting!');
        $output = easylinks_suggest_form();
      }
    } else {
      form_set_error('links_sitename', 'You must enter a valid name for the site you are suggesting!');
      $output = easylinks_suggest_form();
    }
  } else {
    $output = easylinks_suggest_form();
  }
  print theme('page', $output);
}

/**
 * Contains the logic around viewing a list of links for administration.
 */
function easylinks_admin() {
  $header = array(
    array('data' => t('ID')         , 'field' => 'l.lid'       ),
    array('data' => t('Status')     , 'field' => 'l.state'     ),
    array('data' => t('Sugested by')                           ),
    array('data' => t('On')                                    ),
    array('data' => t('Site Name')                             ),
    array('data' => t('Site Desc.') , 'field' => 'l.sitedesc'  ),
    t('Operations')
  );

  $result      = easylinks_select(variable_get('links_rows_in_edit', 10));
  $status      = easylinks_status_list();
  $destination = drupal_get_destination();

  while ($link = db_fetch_object($result)) {
    $username = easylinks_get_username($link->uid);
    $sdttm    = date("D d F Y H:i",$link->sugestdttm);

    $rows[] = array(
      $link->lid,
      $status[$link->state],
      $username,
      $sdttm,
      l($link->sitename, $link->url, array("target" => "_blank")),
      $link->sitedesc,
      l(t('edit'), "easylinks/$link->lid/edit", array(), $destination)
    );
  }

  $pager = theme('pager', NULL, variable_get('links_rows_in_edit', 10), 0);
  if (!empty($pager)) {
    $rows[] = array(array('data' => $pager, 'colspan' => '7'));
  }

  $output = theme('table', $header, $rows);

  print theme('page', $output);
}

/**
 * Contains the logic around viewing a list of links
 */
function easylinks_view() {
  $header = array(t(''), array('data' => t('Site Name')), array('data' => t('Site Desc.')));

  $result      = easylinks_select(variable_get('links_rows_in_view', 10),"state > 0");
  $destination = drupal_get_destination();
  $target      = variable_get('easylinks_target', 0) ? '_blank' : '_self';
  while ($link = db_fetch_object($result)) {
    $rows[] = array(l('<img src="misc/watchdog-error.png" alt="(X)"/>', url("easylinks/$link->lid/reportbroken"), array(), $destination,NULL,FALSE,TRUE),l($link->sitename, $link->url, array(target => $target)), $link->sitedesc);
  }

  $pager = theme('pager', NULL, variable_get('links_rows_in_view', 10), 0);
  if (!empty($pager)) {
    $rows[] = array(array('data' => $pager, 'colspan' => '6'));
  }
  if (count($rows) == 0) {
    drupal_set_message(t('Sorry, No links defined. Please come back later.'), 'message');
    $output = '';
  }
  $output = theme('table', $header, $rows);
  print theme('page', $output);
}


/**
 * Logic for editing a link
 */
function easylinks_admin_edit() {
  drupal_set_title('edit link');

  if($_POST['op'] == t('Update')) {

    easylinks_update($_POST['edit']);
    drupal_set_message(t('Link updated.'));
    drupal_goto("admin/easylinks");

  } else if ($_POST['op'] == t('Delete')) {

    $link = easylinks_build_form_data();
    $form = easylinks_admin_edit_form($link,"readonly");
    $form['Confirm'] = array('#type' => 'submit', '#value' => t('Confirm'));
    drupal_set_message(t('Please confirm that you want to delete this link.'));
    $output = drupal_get_form('easylinks_delete_confirm', $form);

  } else if ($_POST['op'] == t('Confirm')) {

    easylinks_delete($_POST['edit']);
    drupal_set_message(t('Link deleted.'));
    drupal_goto("admin/easylinks");

  } else {

    $result = easylinks_select(variable_get('links_rows_in_edit', 10),"lid = " . arg(1));
    if (db_num_rows($result)) {
      $link   = db_fetch_object($result);
      $form   = easylinks_admin_edit_form($link);
      $form['Update'] = array('#type' => 'submit', '#value' => t('Update'));
      $form['Delete'] = array('#type' => 'submit', '#value' => t('Delete'));
      $output = drupal_get_form('easylinks_links_listing', $form);
    }

  }
  print theme('page',$output);
}

/**
 * Logic for reporting a broken link
 */
function easylinks_report_broken() {
  if($_POST['op'] == t('Report')) {
    $_POST['edit']['links_state'] = 2;
    easylinks_update($_POST['edit']);
    drupal_set_message(t('Thank you for reporting broken link. The site administrator will investigate the problem.'));
    drupal_goto("easylinks");
  } else {
    drupal_set_title('report broken link');

    $result = easylinks_select(1,"lid = " . arg(1));
    if (db_num_rows($result)) {
      $link   = db_fetch_object($result);
      $form   = easylinks_report_broken_form($link);
      $form['Report'] = array('#type' => 'submit', '#value' => t('Report'));
      $output = drupal_get_form('easylinks_report_broken', $form);
    }
  }
  print theme('page',$output);
}

/**
 * Creates a block for viewing last X links
 */
function easylinks_block($op='list', $delta = 0, $edit = array()) {
  switch($op) {
    case 'list':
      $block[0]['info'] = t('Lastest approved easylinks');
      break;
    case 'configure':
    case 'save':
      break;
    case 'view':
      $block['subject'] = t('Latest Links');
      $block['content'] = easylinks_block_1();
      break;
    }

  return $block;
}

function easylinks_block_1() {
  $target  = variable_get('easylinks_target', 0) ? '_blank' : '_self';
  $result  = easylinks_select(variable_get('links_rows_in_block', 10),"state > 0");
  $content = array();
  $i = 0;
  while ($link = db_fetch_object($result)) {
    $content[$i] = l($link->sitename, $link->url, array(target => $target));
    $i++;
  }
  $output = theme('item_list', $content);
  $output .= '<div class="more-link">' . l(t('more links'), drupal_get_path_alias('easylinks')).'</div>';
  return $output;
}