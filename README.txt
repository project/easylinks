Introduction:
-------------

The intention of this module is to facilitate a means by which
drupal users can allow their visitors to suggest links to valid
URL's.

There is basic functionality for reporting a broken link as well as
editing links.

Notes:
------

1. To date, I have been unable to test easylinks on Postgresql. Thus the
   missing easylinks.pgsql file.

   If you have the time and resources to create this file I would greatly
   appreciate it.

2. I have noted a problem under Drupal 4.7.0, where the "easylinks" settings page
   does not show in the menu. I have not been able to get to the bottom of this.
   All seems fine in version 4.7.2.

   Please note that I have not tested under 4.7.1. Chances are that the problem still
   exists in this version as I have seen some comments regarding simmilar symptoms for
   other modules.

3. easylinks has been tested under:
    - MySQL version 4.1.13 / 5.0.21-community-nt
    - PHP version 4.4.0 / 5.1.4
    - Drupal 4.7.3

4. I would appretiate your feedback/comments/flames etc. :)

Original Author:
------
Dirk J. Botha <bothadj@wdsl.co.za>

Current Maintainer:
------
Gurpartap http://drupal.org/user/41470

